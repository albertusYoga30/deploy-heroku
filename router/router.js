const router = require('express').Router();
const auth = require('../controller/authController');
const restrict = require('../middlewares/restrict');

//homepage
router.get('/', (req, res) => res.render('login'));

//register page
router.get('/register', (req, res) => res.render('register'));
router.post('/register', auth.register);

// login
router.get('/login', (req, res) => res.render('login'));
router.post('/login', auth.login);

// whoami
router.get('/', restrict, (req, res) => res.render('whoami'));
router.get('/whoami', restrict, auth.whoami);

module.exports = router;
