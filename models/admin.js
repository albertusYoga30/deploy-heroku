'use strict';
const { Model } = require('sequelize');
const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  class admin extends Model {
    static associate(models) {
      // define association here
    }

    // create user
    static #encrypt = (password) => bcrypt.hashSync(password, 10);
    static register = ({ username, password }) => {
      const encryptedPassword = this.#encrypt(password);
      return this.create({ username, password: encryptedPassword });
    };

    // autentification
    cheskPassword = (password) => bcrypt.compareSync(password, this.password);
    static authenticate = async ({ username, password }) => {
      try {
        const admin = await this.findOne({ where: { username } });
        if (!admin) return Promise.reject('User not found!');

        const isPasswordValid = admin.cheskPassword(password);
        if (!isPasswordValid) return Promise.reject('Wrong Password');
        return Promise.resolve(admin);
      } catch (err) {
        return Promise.reject(err);
      }
    };
  }

  admin.init(
    {
      username: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'admin',
    }
  );
  return admin;
};
