const { message } = require('../models');

module.exports = {
  create: (res, req) => {
    const { name, email, message } = req.body;
    message
      .create({ name, email, message })
      .then((message) =>
        res.json({
          status: 200,
          message: 'Post Berhasil',
          data: message,
        })
      )
      .catch((err) =>
        res.json({
          status: 500,
          message: 'kesalahan server',
        })
      );
  },

  showAll: (res, req) => {
    message
      .findAll({})
      .then((message) => {
        if (!message == 0) {
          res.json({
            status: 200,
            message: 'berhasil',
            data: message,
          });
        } else {
          res.json({
            status: 400,
            message: 'data tidak ditemukan',
          });
        }
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: 'Kesalahan server',
        });
      });
  },
};
