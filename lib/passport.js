const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const { admin } = require('../models');

/* Fungsi untuk authentication */
async function authenticate(username, password, done) {
  try {
    const Admin = await admin.authenticate({ username, password });
    return done(null, Admin);
  } catch (err) {
    /* Parameter ketiga akan dilempar ke dalam flash */
    return done(null, false, { message: err.message });
  }
}

passport.use(new LocalStrategy({ usernameField: 'username', passwordField: 'password' }, authenticate));

// Sessions
passport.serializeUser((admin, done) => done(null, admin.id));
passport.deserializeUser(async (id, done) => done(null, await admin.findByPk(id)));

module.exports = passport;
