const express = require('express');
const session = require('express-session');
const flash = require('express-flash');

const app = express();
const { PORT = 5432 } = process.env.PORT;

app.use(express.urlencoded({ extended: false }));

app.use(
  session({
    secret: 'Buat ini jadi rahasia',
    resave: false,
    saveUninitialized: false,
  })
);

app.use(flash());
app.set('view engine', 'ejs');

const passport = require('./lib/passport');
app.use(passport.initialize());
app.use(passport.session());

const router = require('./router/router');
app.use(router);
app.listen(PORT, () => {
  console.log(`Server at http://localhost:${PORT}`);
});
